# Docker for Traefik

> [Træfɪk](https://docs.traefik.io/) is a modern HTTP reverse proxy and load balancer that makes deploying microservices easy.  
Træfik integrates with your existing infrastructure components ([Docker](https://www.docker.com/), [Swarm mode](https://docs.docker.com/engine/swarm/), [Kubernetes](https://kubernetes.io), [Marathon](https://mesosphere.github.io/marathon/), [Consul](https://www.consul.io/), [Etcd](https://coreos.com/etcd/), [Rancher](https://rancher.com), [Amazon ECS](https://aws.amazon.com/ecs), ...) and configures itself automatically and dynamically.
Telling Træfik where your orchestrator is could be the _only_ configuration step you need to do.
> 
> Sources [here](https://github.com/containous/traefik-library-image)

## Precondition

1. Before building the project, Traefik need a file with right `600` to store all certificates.

```bash
touch config/acme.json
sudo chmod 600 config/acme.json
```

2. In `docker-compose.yml` you must specify a domaine name that pointing to your server.

```yml
...
- traefik.http.routers.traefik.rule=Host(`traefik.mydomaine.com`)
...
```

3. In `config/traefik.yml` you must specify a valid email for `certificatesResolvers`.

## Run

We must build the network in which Traefik will acted. In this config the network is call `traefik_network`. 

```bash
docker network create traefik_network
```

Lastly, you can build the container 🐳

```bash
docker-compose up -d
```

## Config

## docker-compose.yml

- Image: **Traefik 2.2**
- Ports: 
  - 80 → 80 : HTTP
  - 443 → 443 : HTTPS
  - 8091 → 8091 : Ping
- Volumes:
  - [traefik.yml](#traefik.yml)
  - [dynamic-conf.yml](#dynamic-conf.yml)
  - .htpasswd
  - acme.json
  - docker.sock
- Labels:
  - Dashboard **enabled** 
  - TLS **enabled** with **letsencrypt** resolver
  - Entrypoint: [webSecure](#traefik.yml)
  - Middleware: [adminAuth@file](#dynamic-conf.yml)

## traefik.yml

This file is used by traefik to build configurations.  
In this default configuration, entrypoints are `:80` for **web**, `:443` for **webSecure** and `:8091` for **ping**. Certification resolver is [Let's Encrypt](https://letsencrypt.org/). Every certificates are signed with your email and they are store in `acme.json` file.

## dynamic-conf.yml

This file defines more specificity to build a dynamic configuration.  
 - All web entrypoint (port `80`) are redirect to `https` (port `443`) whith a middleware.
  - The `adminAuth` middleware adds a layer security for specified routes. To add users access, you must edit `config/.htpasswd` file (use this [generator](https://www.web2generators.com/apache-tools/htpasswd-generator) to help you)
  - TLS default version is `VersionTLS12`
